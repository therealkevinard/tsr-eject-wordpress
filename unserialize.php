<?php

$fp = fopen(__DIR__ . '/input/gift-cards.csv', 'r');
$out = [];
while ($data = fgetcsv($fp, 1000, ',')) {
    $arr = unserialize($data[2]);
    if($arr){
        $out[] = $arr;
    }
}

echo json_encode($out);