

note on gift cards: 
The keys are buried in wpdb as serialized json 

execute this sql query, collecting its output to a csv file. 
```sql
select user_id, meta_key, meta_value
from wp_usermeta
where meta_key = 'pmpro_gift_codes_purchased_for';
```

to parse `meta_value`, use php's `unserialize()` as in `php ./unserialize.php | tee output/gift-cards.json`. the content of gift-cards.json can then be used in node 