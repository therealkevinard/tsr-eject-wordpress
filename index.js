const fs = require('fs');
const path = require('path');
// const _ = require('lodash');
// const jsonToCsv = require('json2csv').Parser;
const mysql = require('mysql2');
const moment = require('moment');
//----------
const secrets = require('./secrets');
const database = require('./src/database');
const stripeApi = require('./src/stripe/stripe-api-actions');
const cmsHandlers = require('./src/handlers');
const utils = require('./src/utils');
//----------
const {
    User, UserPipeline
} = require('./src/pipelines/users');
const {
    Author, AuthorPipeline
} = require('./src/pipelines/author');
const {
    Post, PostsPipeline,
} = require('./src/pipelines/posts');
const {
    Tag
} = require('./src/pipelines/tag');

//===================================================================================================== Utils
/**
 * async wrapper for sql query
 * @param connection
 *  mysql2 connection instance to act on
 * @param query
 *  parameterized query
 * @param params
 *  query params
 * @return {Promise}
 */
const waitForQuery = async (connection, query, params) => {
    return new Promise((resolve, reject) => {
        connection.execute(
            query,
            params,
            (err, res, fields) => {
                if (err) reject(err);
                resolve({res, fields})
            }
        )
    })
};

//===================================================================================================== Parsers
/**
 * Parse a payment record from csv into usable subscription data
 * @param paymentRow
 * @return {{} & {customer_id: boolean, subscription_id: boolean, next_pay_timestamp: boolean} & {customer_id: boolean, subscription_id: string, next_pay_timestamp: string}}
 */
const subscriptionDataFromRow = (paymentRow) => {
    let subdata = {
        customer_id: false,
        subscription_id: '',
        next_pay_timestamp: false
    };
    return Object.assign({}, subdata, {
        customer_id: paymentRow.customer_id,
        next_pay_timestamp: moment(paymentRow.created_utc).add(1, "years").format('X')
    })
};

//===================================================================================================== Composed Content Routines
/**
 *
 * @return {Promise<{source, target}>}
 */
const prepareDbConn = async () => {
    let wordpress = await database.getDbConnection(secrets.db_etl);
    let october_dev = await database.getDbConnection(secrets.db_localdev);
    return {
        source: wordpress,
        target: october_dev
    }
};

const loadOcEntities = async (target) => {
    let terms = await cmsHandlers.october.fetchTerms(target);
    let issues = await cmsHandlers.october.fetchIssues(target);
    let posts = await cmsHandlers.october.fetchPosts(target);
    let users = await cmsHandlers.october.fetchUsers(target);
    let authors = await cmsHandlers.october.fetchAuthors(target);
    return {
        terms, issues, posts, users, authors
    }
};

//===================================================================================================== ...
const patch_userGiftStatus = async (target) => {
    let users = await cmsHandlers.october.fetchUsers(target)
    users = users.map(row => {
        let u = new User();
        u.mergeOcRecord(row);
        return u;
    });

    const giftrecords = require(path.join(__dirname, 'input', 'gift-cards.json'))
        .reduce((acc, curr) => {
            return [
                ...acc,
                ...curr
            ]
        }, []);

    giftrecords.map(gift => {
        users
            .filter(u => {
                return u.email === gift.buyer.email
            })
            .forEach(u => {
                u.memberSource = 'gift';
                u.memberSourceNotes = 'gift giver'
            })
        users
            .filter(u => {
                return u.email === gift.recipient.email
            })
            .forEach(u => {
                if (!u.stripeCustomerId) {
                    u.memberSource = 'gift';
                    u.memberSourceNotes = 'gift recipient'
                }
            })
    });
    const gift_users = users.filter(u => {
        return u.memberSource === 'gift';
    });

    let tplUpdate = `update users
                     set membership_payment_source       = ?,
                         membership_payment_source_notes = ?
                     where id = ?`;
    let promises = gift_users.map(u => {
        return new Promise(patchResolve => {
            let tplVals = u.getSqlFields(['memberSource', 'memberSourceNotes', 'id']);
            target.query(
                tplUpdate,
                tplVals,
                (err, res, fields) => {
                    patchResolve({err, res, fields})
                }
            );
        })
    });
    let results = await utils.collectAllPromises(promises);

    return Promise.resolve(results);
}

/**
 * Given manually-resolved buyer/recip relation csv, update members accordingly
 */
const patch_giftSubscriptions = async (target) => {
    // first, load october users db as static collection; saves queries later
    let users = await cmsHandlers.october.fetchUsers(target);
    // parse provided csv
    let csvfile = path.resolve(__dirname, 'input', "Gift Subscriptions Worksheet.csv");
    let csvdata = fs.readFileSync(csvfile, 'utf8');
    let records = await utils.parseCsv(csvdata);

    // ...do things with `records`
    const momentToMysql = (m) => {
        return m.format("YYYY-MM-DD HH:mm:ss")
    }
    let userPromises = [];
    records.map(record => {
        let purchase = moment(record.gift_purchase_date, "M/D/YY");
        let expire = moment(purchase).add(1, 'years');

        let gift_giver, gift_recip;
        gift_giver = users.filter(u => {
            return u.email === record.gift_giver_email;
        })[0];
        gift_recip = users.filter(u => {
            return u.email === record.gift_recip_email;
        })[0];

        // update user records
        if (gift_giver) {
            userPromises.push(
                new Promise(userUpdateResolve => {
                    target.query(
                            `update users
                             set membership_payment_source_notes = ?
                             where id = ?`,
                        [
                            gift_recip ? `${gift_giver.membership_payment_source_notes}, gift giver [gifted to ${gift_recip.email} ]` : `gift giver`,
                            gift_giver.id
                        ],
                        (err, result, fields) => {
                            userUpdateResolve({err, result, fields});
                        })
                })
            )
        }
        if (gift_recip) {
            userPromises.push(
                new Promise(userUpdateResolve => {
                    target.query(
                            `update users
                             set membership_payment_source       = 'gift',
                                 membership_payment_source_notes = 'gift recipient',
                                 membership_start                = ?,
                                 membership_end                  = ?
                             where id = ?`,
                        [
                            momentToMysql(purchase),
                            momentToMysql(expire),
                            gift_recip.id
                        ],
                        (err, result, fields) => {
                            userUpdateResolve({err, result, fields})
                        }
                    )
                })
            )
        }

        /*try {
            console.log(
                "\n purch: %s \n expire: %s \n recip: %s %s ",
                momentToMysql(purchase),
                momentToMysql(expire),
                gift_recip.id,
                gift_recip.name
            )
        }
        catch (err) {
        }*/
    });

    await utils.collectAllPromises(userPromises);

    debugger;
}

/**
 * Pipeline for loading all data from all sources, and resolving a diffed dataset for update to the target
 * @param source
 * @param target
 * @param cacheRefresh true: load new data from api, false: read cached fs json
 * @return {Promise}
 */
const pipeline_twoWayResolution = async (source, target, cacheRefresh = false) => {
    let wpEntities = {}; // from wordpress
    let ocEntities = {}; // from october
    let offlineEntites = {}; // from static sources (csv, etc)

    //***************************************************************************** Loading
    /*
    Load ocEntities
     */
    ocEntities = await loadOcEntities(target);
    ocEntities.users = ocEntities.users.map(ocu => {
        let user = new User();
        user.mergeOcRecord(ocu);
        return user;
    });
    ocEntities.terms = ocEntities.terms.map(oct => {
        let tag = new Tag()
        tag.mergeOcRecord(oct);
        return tag
    });

    /*
    Load wpEntities - conditionally, this loads from cache or live db/api
     */
    let usefulEndpoints = [
        'archive-contributor',
        'archive-item',
        'categories',
        'media',
        'pages',
        'posts',
        'tags',
    ];
    let wpc = new cmsHandlers.wordpress.WpClient('https://thesewaneereview.com/wp-json/wp/v2');
    for (let i = 0; i < usefulEndpoints.length; i++) {
        let current = usefulEndpoints[i];
        let cacheFile = path.join(__dirname, 'output', `wp-entities-${current}.json`);
        //
        if (cacheRefresh) {
            wpEntities[current] = await wpc.paginateApiResponse(current, 1);
            fs.writeFileSync(cacheFile, JSON.stringify(wpEntities), null, 4); //-- cache data.
        }
        else {
            let cached = JSON.parse(fs.readFileSync(cacheFile));
            wpEntities = {
                ...wpEntities,
                ...cached
            }
        }
    }
    /*wpEntities.users = await cmsHandlers.wordpress.fetchUsers(source);
    wpEntities.users = wpEntities.users.map(wpu => {
        let user = new User();
        user.mergeWpRecord(wpu);
        return user;
    });*/

    /*
    Stray entities from offline sources
     */
    /*let csvpath = path.join(__dirname, 'input', 'nonweb_subscribers.csv');
    let raw = fs.readFileSync(csvpath, 'utf8');
    let parsed = await parseCsv(raw);
    offlineEntites.users = parsed.map(row => {
        let user = new User();
        user.username = row.email;
        user.address1 = row.address1;
        user.address2 = row.address2;
        user.city = row.city;
        user.country = row.country;
        user.email = row.email;
        user.fname = row.fname;
        user.lname = row.lname;
        user.state = row.state;
        user.zip = row.zip;
        user.password = '---';
        user.activated = 1;

        return user;
    });*/

    //***************************************************************************** Resolution
    //----------------------------------------------------- users
    /*let up = new UserPipeline();
    up.setCollection('october', ocEntities.users);
    up.setCollection('wordpress', wpEntities.users);
    up.setCollection('offline', offlineEntites.users);
    let newUsers = up.scopeNewUsers('october', 'wordpress');*/

    //----------------------------------------------------- authors
    let authorsPipeline = new AuthorPipeline();
    authorsPipeline.setCollection(
        'october',
        ocEntities.authors.map(author => {
            let a = new Author();
            a.mergeOcRecord(author);
            return a;
        })
    );
    authorsPipeline.setCollection(
        'wordpress',
        wpEntities.posts
            .map(post => {
                let a = new Author();
                a.mergeWpRecord({
                    id: null,
                    name: post['Author'] ? post['Author'][0] : '',
                    bio: post['Author Bio, Brief'] ? post['Author Bio, Brief'][0] : '',
                })
                return a;
            })
            .reduce((acc, curr) => {
                let found = null;
                acc.forEach((author, i) => {
                    found = author.name === curr.name ? i : found;
                });
                return found ?
                    acc :
                    [
                        ...acc,
                        curr
                    ]
            }, [])
    );

    let newAuthors = authorsPipeline.scopeNew('october', 'wordpress');
    if (newAuthors.length > 0) {
        let insertedAuthors = await cmsHandlers.october.insertAuthors(target, newAuthors);
        // brutish, i know - re-process oc authors into the pipeline after inserts
        ocEntities.authors = await cmsHandlers.october.fetchAuthors(target);
        authorsPipeline.setCollection(
            'october',
            ocEntities.authors.map(author => {
                let a = new Author();
                a.mergeOcRecord(author);
                return a;
            })
        );
    }

    //----------------------------------------------------- posts
    let postsPipeline = new PostsPipeline();
    postsPipeline.setCollection(
        'october',
        ocEntities.posts
    );

    postsPipeline.setCollection(
        'wordpress',
        wpEntities.posts
            .map(post => {
                let p = new Post();
                p.mergeWpRecord(post);
                if (post['Author']) {
                    p.author = authorsPipeline.collections['october']
                        .filter(author => {
                            return author.name === post['Author'][0];
                        })[0]
                }
                return p;
            })
    );
    let newPosts = postsPipeline.scopeNew('october', 'wordpress');
    if (newPosts.length > 0) {
        await cmsHandlers.october.insertPosts(target, newPosts)
        ocEntities.posts = await cmsHandlers.october.fetchPosts(target)
        postsPipeline.setCollection(
            'october',
            ocEntities.posts
        )
    }
    debugger;


    return Promise.resolve(true);
};


//----------------------------------------------------------------------------------------------------- Entrypoint
(async () => {
    let {
        source, target
    } = await prepareDbConn(); // prepare db connections

    let outcome = await pipeline_twoWayResolution(source, target, false);
    // let patch = await patch_userGiftStatus(target);
    // let patch = await patch_giftSubscriptions(target)
    return Promise.resolve(true);
})();