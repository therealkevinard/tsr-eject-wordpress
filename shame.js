/* **********************************************************
 * @deprecated @shame
 * This was used in run-n-gun code sessions, so it's all. over. the. place. Leaving as a general reference of
 * how to work with this tool, and an illustration of worst practices ever.
 ********************************************************** */

let upgradeMembers = async (planId, csvInputPath) => {
    console.time('prepare');
    let raw = fs.readFileSync(csvInputPath, 'utf8');
    let parsedPayments = await parseCsv(raw);
    parsedPayments = parsedPayments.filter(cus => {
        return cus.status === "Paid"
    });
    // ---------------------------------------------------------------------- Manage any users who already have the target plan
    // how many gateway customers already have this plan?
    let usersWithModernPlan = await stripeApi.customersByPlan(planId);
    usersWithModernPlan = usersWithModernPlan.map(cus => {
        return cus.customer;
    });
    console.log(
        '%d are already assigned %s',
        ...[usersWithModernPlan.length, planId]
    );

    // ---------------------------------------------------------------------- Who does NOT? (will be acted on)
    // how many gateway customers will be granted this plan?
    let userUpgradeCount = parsedPayments.reduce((prev, curr) => {
        let subscriptionData = subscriptionDataFromRow(curr);
        let offset = subscriptionData.customer_id.indexOf('cus_') === 0 ? 1 : 0;
        return prev + offset;
    }, 0);
    console.log(`preparing to assign %s to [%d] customers`, ...[planId, userUpgradeCount]);

    // ---------------------------------------------------------------------- Report Metrics
    let customerTally = parsedPayments.reduce((acc, curr) => {
        if (typeof acc[curr.customer_email] === 'undefined') {
            acc[curr.customer_email] = 0;
        }
        acc[curr.customer_email] = acc[curr.customer_email] + 1;
        return acc;
    }, {});

    let rePayers = Object.keys(customerTally)
        .reduce((acc, curr) => {
            if (customerTally[curr] > 1) {
                acc[curr] = customerTally[curr];
            }
            return acc;
        }, {});

    Object.keys(rePayers)
        .map(c => {
            rePayers[c] = parsedPayments.filter(p => p.customer_email === c);
        });

    // ---------------------------------------------------------------------- Normalize Dataset
    // 1. Flatten duplicates, keeping most recent (the csv is pre-sorted)
    let normalized = parsedPayments.reduce((acc, curr) => {
        if (!curr.customer_email || curr.customer_email === '') {
            return acc
        }
        return {
            ...acc,
            [curr.customer_email]: curr
        }
    }, {});
    let tmp = Object.keys(normalized).map(cus => normalized[cus]);
    normalized = tmp;

    // 2. Remove customers who already have this subscription
    normalized = normalized.filter(cus => {
        return usersWithModernPlan.indexOf(cus.customer_id) === -1;
    });

    console.log(
        'After flattening customers by email and removing those who already have the modern plan, have [%d] customers that will be assigned the %s plan',
        ...[normalized.length, planId]
    );
    console.timeEnd('prepare');
    //----------------------------------------------------------------------

    console.time('assignments');
    //-- attach subscriptions to actual customers
    for (let i = 0; i < normalized.length; i++) {
        let subscriptionData = subscriptionDataFromRow(normalized[i]);
        subscriptionData.subscription_id = planId; // moving planId here is more sustainable.
        try {
            await stripeApi.attachSubscription(subscriptionData); // <-- this is where it happens
            console.log(`assigned: [%d/%d]`, i, normalized.length)
        } catch (ex) {
            console.log(ex)
        }
    }
    console.timeEnd('assignments');
    return new Promise(resolve => {
        resolve(true)
    })
};
let confirmCustomerNumbers = async (csvInputPath) => {

    const conn = mysql.createConnection({
        host: 'localhost',
        port: '3308',
        user: 'root',
        password: 'dockerpass',
        database: 'swndb'
    });
    conn.connect();

    let raw = fs.readFileSync(csvInputPath, 'utf8');
    let upgradedCustomers = await parseCsv(raw);

    /*
    audit: customer active/inactive (master list vs site-side database)
     */
    /*
        let fetched = {
            active: [],
            inactive: []
        };

        for (let i = 0; i < upgradedCustomers.length; i++) {
            let c = upgradedCustomers[i];
            let {res} = await waitForQuery(
                conn,
                "select * from users where email = ? and offline_cashier_stripe_id = ?",
                [c.customer_email, c.customer_id]
            );
            try {
                console.log(res[0].member_status, c.customer_email);
                let coll = res[0].member_status === 'active' ? fetched.active : fetched.inactive;
                coll.push(res[0]);
            } catch (err) {
            }
        }
    */

    /*
    audit: all customer numbers from master list are accounted for in siteside db?
    customerNumberCounts.one: ideal; the cus_ has a single attached user record.
    customerNumberCounts.less: cus_ has too-few attached users
    customerNumberCounts.more: cus_ has too many attached users
     */
    let customerNumberCounts = {
        one: [],
        more: [],
        less: [],
    };
    for (let i = 0; i < upgradedCustomers.length; i++) {
        let cus = upgradedCustomers[i];
        let {res} = await waitForQuery(
            conn,
            "select * from users where offline_cashier_stripe_id = ?",
            [cus.customer_id]
        );

        let bucket = customerNumberCounts.one;
        bucket = res.length > 1 ? customerNumberCounts.more : bucket;
        bucket = res.length < 1 ? customerNumberCounts.less : bucket;
        bucket.push(cus);
    }

    /*
    customerNumberCounts.less: do the unaccounted have user records at all (for their email) ?
    foundUsers.found: ideal, the cus_ associated email is in siteside db
    foundUsers.lost: the cus_ associated email isn't here.
     */
    let foundUsers = {
        found: [],
        lost: [],
    };
    for (let i = 0; i < customerNumberCounts.less.length; i++) {
        let {res} = await waitForQuery(
            conn,
            "select * from users where email = ?",
            [customerNumberCounts.less[i].customer_email]
        );

        if (res.length > 0) {
            foundUsers.found = [...foundUsers.found, ...res] // using spread because it's not impossible for res.lenth > 1
        }
        else {
            foundUsers.lost.push(customerNumberCounts.less[i])
        }
    }

    return new Promise(resolve => {
        resolve({
            customerNumberCounts,
            foundUsers
        })
    })
};

/**
 * Parse a user object into usable subscription data
 * @param customerOb
 * @return {{} & {customer_id: boolean, subscription_id: boolean, next_pay_timestamp: boolean} & {customer_id: *, subscription_id: string, next_pay_timestamp: string}}
 */
const subscriptionDataFromCustomerOb = (customerOb) => {
    let subdata = {
        customer_id: false,
        subscription_id: '',
        next_pay_timestamp: false
    };
    let lastPaymentDate = "2018-10-05 19:01"; // @todo find this somewhere _real_
    return Object.assign({}, subdata, {
        customer_id: customerOb.id,
        next_pay_timestamp: moment(lastPaymentDate).add(1, "years").format('X')
    })
};

const loadWpEntities = async (source) => {
    let wpPosts = await cmsHandlers.wordpress.fetchPosts(source, 'post');
    let wpUsers = await cmsHandlers.wordpress.fetchUsers(source);

    return {
        wpPosts, wpUsers
    }
};

/**
 * @param source
 * @param target
 * @return {Promise}
 */
const scratchpad = async (source, target) => {
    let {wpPosts, wpUsers} = await loadWpEntities(source);
    let {ocTerms, ocIssues, ocPosts, ocUsers} = await loadOcEntities(target);

    //----------------------------------------------------------------------------------------------------- upgrade memberships
    // let planId = 'tsr-annual-recurring';
    // let csvInputPath = path.join(path.resolve(), 'input', '365daypayments_20181016.csv');
    // let csvOutputPath = path.join(path.resolve(), 'output', 'will-upgrade-subscription.csv');
    // let status = await upgradeMembers(planId, csvInputPath);
    //-----------------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------------- confirm subscriptions after-the-fact
    // let planId = 'tsr-annual-recurring';
    // let csvInputPath = path.join(path.resolve(), 'output', 'upgraded-subscriptions.csv');
    // let status = await confirmCustomerNumbers(csvInputPath);
    // console.log(status)
    //-----------------------------------------------------------------------------------------------------

    //===================================================================================================== Add new entities to staging db

    //----------------------------------------------------------------------------------------------------- Posts / Authors / Categories
    /*
    //-- identify new posts
    let newPosts = wpPosts.filter(wpp => {
        return ocPosts.filter(ocp => {
            return ocp.title === wpp.post.post_title
        }).length === 0;
    });
    //-- identify new authors
    let wpAuthors = newPosts.map(wpp => {
        return wpp.author
    });
    let ocAuthors = await cmsHandlers.october.fetchAuthors(target);
    let newAuthors = wpAuthors
        .filter(wpa => {
            return ocAuthors.filter(oca => {
                return oca.name === wpa.name;
            }).length === 0
        })
        .filter(a => {
            return a.name ? true : false; // explicitly remove empties
        })

    //-- insert new authors
    if (newAuthors.length) {
        await cmsHandlers.october.insertAuthors(target, newAuthors);
        ocAuthors = await cmsHandlers.october.fetchAuthors(target); // better way? ofc there is.
    }
    //-- insert new posts
    if (newPosts.length) {
        await cmsHandlers.october.insertPosts(target, newPosts);
        ocPosts = await cmsHandlers.october.fetchPosts(target);
        //-- update ri
        let patchAuthors = [];
        let patchCategories = [];

        newPosts.forEach(wpPost => {
            let ocSyncPost = ocPosts.filter(ocp => {
                return ocp.title === wpPost.post.post_title;
            })[0]; // match wpPost with its corresponding ocPost
            let ocSyncAuthor = ocAuthors.filter(oca => {
                return oca.name === wpPost.author.name;
            })[0] // match with the corresponding author
            let ocSyncTerms = wpPost.terms
                .filter(wpt => {
                    return wpt.term_type === 'category' ? true : false
                })
                .map(wpt => {
                    return ocTerms.filter(oct => {
                        return wpt.term_name === oct.name ? true : false;
                    })[0];
                })
                .map(syncTerm => {
                    return {
                        postId: ocSyncPost.id,
                        categoryId: syncTerm.id
                    }
                }) // build an array to relate post.id with category.id
            // push new author id
            if(ocSyncAuthor){
                patchAuthors.push(new Promise(async resolve => {
                    let tplUpdate = `update rainlab_blog_posts
                                 set author_id = ?
                                 where id = ?`;
                    let tplValues = [ocSyncAuthor.id, ocSyncPost.id];
                    target.query(
                        tplUpdate,
                        tplValues,
                        (err, res, fields) => {
                            if (err) throw err
                            resolve({res, fields})
                        }
                    )
                }));
            }
            if(ocSyncTerms){
                patchCategories.push(new Promise(async resolve => {
                    await cmsHandlers.october.loadPostCatMap(target, ocSyncTerms);
                    resolve(true)
                }))
            }
        });
        await Promise.all(patchAuthors);
        await Promise.all(patchCategories);
    }
*/

    //----------------------------------------------------------------------------------------------------- Users
    let newUsers = wpUsers.filter(wpu => {
        return ocUsers.filter(ocu => {
            return ocu.email === wpu.userEntity.user_email;
        })
    })

    return Promise.resolve(newUsers);
}

const deserialize = () => {
    debugger;
}

deserialize();