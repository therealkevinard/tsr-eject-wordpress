/**
 * Defaults/Template for secrets (in ./secret.js)
 */
const secretSecrets = require('./secret');
const publicSecrets = {
    stripeTokens: {
        test: {
            key: 'pk_test_xxxxxxxxxxxxxxxxxxxxxxxx',
            secret: 'sk_test_xxxxxxxxxxxxxxxxxxxxxxxx',
        },
        live: {
            key: 'pk_live_xxxxxxxxxxxxxxxxxxxxxxxx',
            secret: 'sk_live_xxxxxxxxxxxxxxxxxxxxxxxx',
        }
    },
    db_staging: {
        host: '127.0.0.1',
        port: 1000,
        database: '----------',
        user: '----------',
        password: '----------'
    },
    db_localdev: {
        host: '127.0.0.1',
        port: 1000,
        database: '----------',
        user: '----------',
        password: '----------'
    }
}

const secrets = Object.assign(
    {},
    publicSecrets,
    secretSecrets
)

const getSecret = (key) => {
    return secrets[key];
}

module.exports = {
    ...secrets,
    getSecret
};