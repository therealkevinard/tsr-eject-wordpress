const AbstractBase = require("./abstract-etl-base");

class User extends AbstractBase {
  constructor() {
    super();

    this.id = 0;
    this.fname = "";
    this.lname = "";
    this.email = "";
    this.password = "";
    this.activated = 1;
    this.activatedAt = new Date();
    this.username = "";
    this.isSuper = 0;
    this.stripeCustomerId = "";
    this.address1 = "";
    this.address2 = "";
    this.city = "";
    this.state = "";
    this.zip = "";
    this.country = "";
    this.memberSource = "";
    this.memberStatus = "";
    this.memberSourceNotes = "";
  }

  /**
   * Assign instance values from incoming wp user record
   * @param wpUser
   */
  mergeWpRecord(wpUser) {
    if (
      wpUser.userMetadata.subscriber_fname &&
      wpUser.userMetadata.subscriber_lname
    ) {
      this.fname = wpUser.userMetadata.subscriber_fname;
      this.lname = wpUser.userMetadata.subscriber_lname;
    } else {
      [this.fname, this.lname] = wpUser.userEntity.display_name.split(" ");
    }
    this.fname = typeof this.fname === "undefined" ? " " : this.fname;
    this.lname = typeof this.lname === "undefined" ? " " : this.lname;
    //--
    this.email = this.username = wpUser.userEntity.user_email;
    this.password = "---";
    this.activated = 1;
    this.activatedAt = new Date();
    //--
    this.stripeCustomerId = wpUser.userMetadata.pmpro_stripe_customerid;
    this.address1 = wpUser.userMetadata.subscriber_address;
    this.address2 = wpUser.userMetadata.subscriber_address_2;
    this.city = wpUser.userMetadata.subscriber_city;
    this.state = wpUser.userMetadata.subscriber_state;
    this.zip = wpUser.userMetadata.subscriber_zip;
    this.country = wpUser.userMetadata.subscriber_country;
  }

  mergeOcRecord(ocUser) {
    this.id = ocUser.id;
    if (ocUser.name && ocUser.surname) {
      this.fname = ocUser.name;
      this.lname = ocUser.surname;
    } else {
      [this.fname, this.lname] = ocUser.name.split(" ");
    }

    this.email = ocUser.email;
    this.password = ocUser.password;
    this.activated = ocUser.is_activated;
    this.activatedAt = ocUser.activated_at;
    this.username = ocUser.username;
    this.isSuper = ocUser.is_superuser;
    this.stripeCustomerId = ocUser.offline_cashier_stripe_id;
    this.address1 = ocUser.address_1;
    this.address2 = ocUser.address_2;
    this.city = ocUser.city;
    this.state = ocUser.state;
    this.zip = ocUser.zip;
    this.country = ocUser.country;
    this.memberSource = ocUser.membership_payment_source;
    this.memberStatus = ocUser.member_status;
    this.memberSourceNotes = ocUser.membership_payment_source_notes;
  }

  /**
   * Return sql-able query params.
   * @param forFields
   */
  getSqlFields(forFields) {
    return forFields.map(f => {
      return this[f];
    });
  }
}

module.exports = User;
