const AbstractEtl = require("./abstract-etl-base");

class Post extends AbstractEtl {
  constructor() {
    super();
    
    this.id = 0; 
    this.user_id = 1; 
    this.title = "";
    this.slug = ""; 
    this.excerpt = ""; 
    this.content = ""; 
    this.content_html = ""; 
    this.published_at = ""; 
    this.created_at = ""; 
    this.updated_at = ""; 
    this.excerpt_long_preview = ""; 
    this.excerpt_long_preview_html = ""; 
    this.excerpt_short_preview = ""; 
    this.excerpt_short_preview_html = "";
    this.typo_style = '';
    this.member_content = 1;
    this.author_id = '';
    this.issue_id = '';
    this.is_archival = 0;
  }
}

module.exports = Post;
