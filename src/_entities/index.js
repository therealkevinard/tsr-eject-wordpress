const User = require("./user");
const Tag = require("./tag");
const Post = require("./post");

module.exports = {
  User,
  Tag,
  Post
};
