const getPublishedContentByType = (wptableprefix, type) => {
  return `
        SELECT
          posts.post_type,
          posts.post_status,
          posts.*
        from ${wptableprefix}_posts posts
        where post_status = 'publish'
        and post_type = '${type}';`;
};

const getPostMetaRowsForId = (wptableprefix, id) => {
  return `
            SELECT
              meta.*
            from ${wptableprefix}_posts posts
            LEFT JOIN ${wptableprefix}_postmeta meta ON meta.post_id = posts.ID
            where posts.ID = ${id};`;
};

const getTerms = wptableprefix => {
  return `SELECT * from ${wptableprefix}_terms posts;`;
};

module.exports = {
  getPublishedContentByType,
  getTerms,
  getPostMetaRowsForId
};
