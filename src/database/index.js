const { getSecret, secrets } = require("../../secrets");
const mysql = require("mysql");

/**
 * Get connection to mysql
 * @param params
 * @return {Promise}
 */
const getDbConnection = async params => {
  return new Promise(resolve => {
    let connection = mysql.createConnection(params);
    connection.connect(err => {
      if (err) throw err;
      resolve(connection);
    });
  });
};

/**
 * Promisified connection.query
 * @param query
 * @param connection
 * @return {Promise}
 */
const queryWrapper = async (query, connection) => {
  return new Promise((resolve, reject) => {
    connection.query(query, (err, results, fields) => {
      if (err) reject(err);
      resolve(results);
    });
  });
};

/**
 * Helper fn: Formats mysql timestamp
 * @param date
 * @return {string}
 */
const formatMysqlTimestamp = date => {
  let d = new Date();
  if (date) {
    d = new Date(date);
  }
  return d
    .toISOString()
    .slice(0, 19)
    .replace("T", " ");
};

module.exports = {
  getDbConnection,
  formatMysqlTimestamp,
  queryWrapper
};
