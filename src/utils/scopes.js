//===================================================================================================== Data Scopes: filters for correlating datasets.


const scopeNewPosts = (setA, setB) => {
    return setA.filter(wpPost => {
        let inOc = setB.filter(ocPost => {
            return ocPost.title === wpPost.title.rendered;
        });
        return inOc.length === 0;
    });
};

module.exports = {
    scopeNewPosts
};
