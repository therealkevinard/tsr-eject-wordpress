const html2md = require("html-markdown");
const {collectAllPromises} = require("../utils");
const {formatMysqlTimestamp} = require("../database");

//----------------------------------------------- Extractors
const fetchTerms = async localdev => {
    return new Promise(resolve => {
        let q = `select id, name, slug
                 from rainlab_blog_categories`;
        localdev.query(q, (err, results) => {
            if (err) throw err;
            resolve(results);
        });
    });
};
const fetchIssues = async localdev => {
    return new Promise(resolve => {
        let q = `select *
                 from trka_tsrliteraryextensions_issues`;
        localdev.query(q, (err, results) => {
            if (err) throw err;
            resolve(results);
        });
    });
};
const fetchAuthors = async localdev => {
    return new Promise(resolve => {
        let q = `select *
                 from trka_tsrliteraryextensions_authors`;
        localdev.query(q, (err, results) => {
            if (err) throw err;
            resolve(results);
        });
    });
};
const fetchPosts = async localdev => {
    return new Promise(resolve => {
        let q = `select *
                 from rainlab_blog_posts`;
        localdev.query(q, (err, results) => {
            if (err) throw err;
            resolve(results);
        });
    });
};
const fetchUsers = async localdev => {
    return new Promise(resolve => {
        let q = `select *
                 from users`;
        localdev.query(q, (err, results) => {
            if (err) throw err;
            resolve(results);
        });
    });
};

//----------------------------------------------- Loaders
const insertTerms = async (localdev, terms) => {
    return new Promise(async resolve => {
        let tplInsert = `INSERT into rainlab_blog_categories (name, slug, parent_id, nest_left, nest_right, nest_depth, created_at, updated_at)
                         VALUES (?, ?, ?, ?, ?, ?, ?, ?);`;
        let prom_termInserts = terms.category.map(cat => {
            return new Promise(termResolve => {
                let now = formatMysqlTimestamp();
                let tplValues = [cat.term_name, cat.term_slug, 1, 1, 2, 1, now, now];
                localdev.query(tplInsert, tplValues, (err, results, fields) => {
                    if (err) throw err;
                    termResolve({
                        results,
                        fields
                    });
                });
            });
        });
        let results = await collectAllPromises(prom_termInserts);
        resolve(results);
    });
};
const insertIssues = async (localdev, issues) => {
    return new Promise(async resolve => {
        let tplInsert = `insert into trka_tsrliteraryextensions_issues (name, volume, number, season, year, slug, featured_articles, published_at, current_issue, published, navigation)
                         values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`;
        let now = formatMysqlTimestamp();
        let prom_issueInserts = issues.map(issue => {
            return new Promise(issueResolve => {
                let tplValues = [
                    issue.name,
                    issue.volume,
                    issue.number,
                    issue.season,
                    issue.year,
                    `${issue.season.toLowerCase()}-${issue.year}`,
                    "[]",
                    now,
                    0,
                    1,
                    "[]"
                ];
                localdev.query(tplInsert, tplValues, (err, results, fields) => {
                    if (err) throw err;
                    issueResolve({
                        results,
                        fields
                    });
                });
            });
        });
        let results = await collectAllPromises(prom_issueInserts);
        resolve(results);
    });
};
const insertAuthors = async (localdev, authors) => {
    return new Promise(async resolve => {
        let tplInsert = `insert into trka_tsrliteraryextensions_authors (name, bio_html)
                         VALUES (?, ?)`;
        let prom_authorInserts = authors.map(author => {
            return new Promise(authorResolve => {
                let tplValues = [
                    author.name || "",
                    author.bio ? `<p>${author.bio}</p>` : ""
                ];
                localdev.query(tplInsert, tplValues, (err, results, fields) => {
                    if (err) throw err;
                    authorResolve({
                        results,
                        fields
                    });
                });
            });
        });
        let results = await collectAllPromises(prom_authorInserts);
        resolve(results);
    });
};
const insertPosts = async (localdev, posts) => {
    return new Promise(async resolve => {
        let tplInsert = `INSERT into rainlab_blog_posts (user_id, title, slug, content, content_html, published_at, published, created_at, updated_at, author_id, typo_style, member_content, issue_id)
                         VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`;
        let prom_postInserts = posts.map(post => {
            let now = formatMysqlTimestamp();
            return new Promise(postResolve => {
                let tplValues = [
                    post.user_id,
                    post.title,
                    post.slug,
                    post.content_html,
                    post.content_html,
                    now,
                    1,
                    now,
                    now,
                    post.author.id,
                    "",
                    1,
                    0
                ];
                localdev.query(tplInsert, tplValues, (err, results, fields) => {
                    if (err) {
                        console.error(err);
                        postResolve(false)
                    }
                    postResolve(true);
                });
            });
        });
        let results = await collectAllPromises(prom_postInserts);
        resolve(results);
    });
};

/**
 * Oc post categories are relational. draws the map table
 * @param localdev
 * @param pstCatMap
 *  []{postId, categoryId}
 * @return {Promise}
 */
const loadPostCatMap = async (localdev, postCatMap) => {
    return new Promise(async resolve => {
        let tplInsert = `insert into rainlab_blog_posts_categories (post_id, category_id)
                         VALUES (?, ?)`;
        let postCatMapInserts = [];

        postCatMap.forEach(pcm => {
            postCatMapInserts.push([pcm.postId, pcm.categoryId]);
        });

        let prom_postcatInserts = postCatMapInserts.map(postCatMap => {
            return new Promise(postcatResolve => {
                let tplValues = postCatMap;
                localdev.query(tplInsert, tplValues, (err, results, fields) => {
                    if (err) console.log(err);
                    postcatResolve(results);
                });
            });
        });
        let results = await collectAllPromises(prom_postcatInserts);
        resolve(results);
    });
};

/**
 * maps/executes a coll of user entities into oc user inserts
 * @param localdev
 * @param users
 * @return {Promise}
 */
const insertUsers = async (localdev, users) => {
    return new Promise(async resolve => {
        let tplInsert = `insert into users (username, name, surname, email, address_1, address_2, city, state_region, postcode, country, offline_cashier_stripe_id, password, is_activated)
                         values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
        let prom_userInserts = users.map(u => {
            return new Promise(userResolve => {
                let tplValues = u.getSqlFields([
                    "username",
                    "fname",
                    "lname",
                    "email",
                    "address1",
                    "address2",
                    "city",
                    "state",
                    "zip",
                    "country",
                    "stripeCustomerId",
                    "password",
                    "activated"
                ]);
                localdev.query(tplInsert, tplValues, (err, results, fields) => {
                    userResolve({err, results, fields}); // don't reject - instead, resolve includes the err for later checks
                });
            });
        });
        let results = await collectAllPromises(prom_userInserts);
        resolve(results);
    });
};

//-----------------------------------------------
module.exports = {
    fetchTerms,
    fetchIssues,
    fetchPosts,
    fetchAuthors,
    fetchUsers,
    insertTerms,
    insertPosts,
    loadPostCatMap,
    insertAuthors,
    insertIssues,
    insertUsers
};
