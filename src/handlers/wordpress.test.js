const { WpClient } = require("./wordpress");

describe("wordpress client", () => {
  let wpc;
  let base = "http://www.google.com";

  beforeEach(() => {
    wpc = new WpClient(base);
  });

  test("respects baseurl", () => {
    expect(wpc.baseUrl).toBe(base);
  });

  test("axios works", async () => {
    let res = await wpc.client.get(base);
    expect(res.status).toBe(200);
    expect(res.statusText).toBe("OK");
  });

  test("creates valid endpoints", () => {
    ["foo", "bar", "baz"].forEach(s => {
      expect(wpc.buildUrl(s)).toBe(`${base}/${s}`);
    });
  });
});
