const octoberHandlers = require("./october");
const wordpressHandlers = require("./wordpress");

module.exports = {
  october: octoberHandlers,
  wordpress: wordpressHandlers
};
