class PipelineBase {
    constructor() {
        this.collections = {};
    }

    /**
     * Assign a collection of objects to the specified key
     * @param key
     *  key name for this collection
     * @param items
     *  collection of user objects. since the source can be any number of formats, normalize to correct class beforehand
     */
    setCollection(key, items) {
        this.collections[key] = items
    }

    /**
     * Get list of collection keys stored in the pipeline
     * @return {string[]}
     */
    collectionKeys() {
        return Object.keys(this.collections);
    }
}

module.exports = PipelineBase;