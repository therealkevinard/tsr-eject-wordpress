const AbstractEtl = require("./abstract-etl-base");

class Tag extends AbstractEtl {
    constructor() {
        super();
        this.id = 0;
        this.name = "";
        this.slug = "";
    }

    mergeWpRecord(wpTag) {
    }

    mergeOcRecord(ocTag) {
        this.id = ocTag.id;
        this.name = ocTag.name;
        this.slug = ocTag.slug;
    }
}

module.exports = {
    Tag
};
