const PipelineBase = require('./pipelinebase');
const AbstractEtl = require("./abstract-etl-base");

class AuthorPipeline extends PipelineBase {
    constructor() {
        super()
    }
    scopeNew(baselineKey, comparisonKey) {
        let baseline = this.collections[baselineKey];
        let comparison = this.collections[comparisonKey];

        return comparison.filter(comparisonUser => {
            let inBaseline = baseline.filter(baseUser => {
                return comparisonUser.name === baseUser.name;
            });
            return inBaseline.length === 0;
        });
    }
}

class Author extends AbstractEtl {
    mergeOcRecord(ocRecord) {
        this.id = ocRecord.id;
        this.name = ocRecord.name;
        this.bio = ocRecord.bio_html
    };
    mergeWpRecord({id, name, bio}) {
        this.id = id;
        this.name = name;
        this.bio = bio;
    }
}

module.exports = {
    Author, AuthorPipeline
}