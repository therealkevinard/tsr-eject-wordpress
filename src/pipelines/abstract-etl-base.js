class AbstractEtlBase {
  mergeWpRecord(wpRec) {}

  mergeOcRecord(ocRec) {}

  /**
   * Return sql-able query params.
   * @param forFields
   */
  getSqlFields(forFields) {
    return forFields.map(f => {
      return this[f];
    });
  }
}

module.exports = AbstractEtlBase;
