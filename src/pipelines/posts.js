const PipelineBase = require('./pipelinebase');
const AbstractEtl = require("./abstract-etl-base");
const {Author} = require('./author');

/**
 *
 */
class PostsPipeline extends PipelineBase {
    scopeNew(baselineKey, comparisonKey) {
        let baseline = this.collections[baselineKey];
        let comparison = this.collections[comparisonKey];

        return comparison.filter(comp => {
            return baseline
                .filter(base => {
                    return comp.title === base.title;
                })
                .length === 0;
        });
    }

    scopePresent(baselineKey, comparisonKey) {

    }
}

/**
 *
 */
class Post extends AbstractEtl {
    constructor() {
        super();
        this.id = 0;
        this.user_id = 1;
        this.title = "";
        this.slug = "";
        this.excerpt = "";
        this.content = "";
        this.content_html = "";
        this.published_at = "";
        this.created_at = "";
        this.updated_at = "";
        this.excerpt_long_preview = "";
        this.excerpt_long_preview_html = "";
        this.excerpt_short_preview = "";
        this.excerpt_short_preview_html = "";
        this.typo_style = '';
        this.member_content = 1;
        this.author_id = '';
        this.issue_id = '';
        this.is_archival = 0;

        this.author = null;
    }

    mergeWpRecord(wpPost) {
        this.wpid = wpPost.id;
        // this.categories = '' // @todo wp stores ids in []wpPost.categories
        // this.tags = '' //@todo: wp stores ids in []wpPost.tags
        this.content_html = wpPost.content.rendered;
        this.published_at, this.created_at, this.updated_at = wpPost.modified;
        this.excerpt, this.excerpt_long_preview_html, this.excerpt_short_preview_html = wpPost.excerpt.rendered;
        this.slug = wpPost.slug;
        this.title = wpPost.title.rendered;
    }
}

module.exports = {
    Post, PostsPipeline,
};