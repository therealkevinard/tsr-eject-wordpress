const stripeClient = require("./stripe-client");
const moment = require("moment");
const secrets = require("../../secrets");
//--------------------------------------------------------- UTILITIES
/**
 * like bluebird, but why pull in bluebird just for this? :D
 * @param request
 * @return {Promise}
 */
const wrapStripeRequest = async request => {
  return new Promise((resolve, reject) => {
    request
      .catch(err => {
        reject(err);
      })
      .then(response => {
        resolve(response);
      });
  });
};

//--------------------------------------------------------- API ACTIONS
/**
 * Collect payments in the past 365 days. This is roughly equiv. to the snapshot csv report pulled from dashboard.stripe.com
 * @return {Promise}
 */
const reportCharges = async () => {
  let reportStart = moment()
    .subtract(1, "years")
    .format("X");
  let limit = 10000;
  let query = {
    limit,
    created: {
      gte: reportStart
    }
  };
  return wrapStripeRequest(
    stripeClient.charges.list(query).autoPagingToArray({ limit })
  );
};

/**
 * Get available plans
 * @return {Promise}
 */
const reportActivePlans = async () => {
  return wrapStripeRequest(stripeClient.plans.list({}));
};

/**
 * Testing: Purges all users in the provided array.
 * Naturally **USE WITH CAUTION**
 * @param usersArray
 * @return {Promise<*>}
 */
const purgeUsers = async usersArray => {
  return false;
  usersArray = usersArray.map(user => {
    return new Promise(resolve => {
      stripeClient.customers
        .del(user.id)
        .catch(err => {
          throw err;
        })
        .then(res => {
          resolve(res);
        });
    });
  });
  return new Promise(async resolve => {
    let results = await Promise.all(usersArray);
    resolve(results);
  });
};

/**
 * Testing: Find all customers by provided email. works well with purgeUsers to tidy the testbed
 * @param email
 * @return {Promise<*>}
 */
const customersByEmail = async email => {
  return wrapStripeRequest(
    stripeClient.customers.list({ email }).autoPagingToArray({ limit: 10000 })
  );
};

/**
 * Queries for users who have a given plan id
 * @param planId
 * @return {Promise}
 */
const customersByPlan = async planId => {
  return wrapStripeRequest(
    stripeClient.subscriptions
      .list({ plan: planId })
      .autoPagingToArray({ limit: 10000 })
  );
};

/**
 * Queries for customer by given cus_* id
 * @param customerId
 * @return {Promise<void>}
 */
const customersById = async customerId => {
  return wrapStripeRequest(stripeClient.customers.retrieve(customerId));
};

/**
 * Attach the testing subscription to a customer (by id)
 * @param customer_id
 * @param subscription_id
 * @param next_pay_timestamp
 * @return {Promise<*>}
 */
const attachSubscription = async ({
  customer_id,
  subscription_id,
  next_pay_timestamp
}) => {
  return wrapStripeRequest(
    stripeClient.subscriptions.create({
      customer: customer_id,
      billing_cycle_anchor: next_pay_timestamp,
      trial_end: next_pay_timestamp,
      items: [{ plan: subscription_id }]
    })
  );
};

const detachSubscripiton = async ({ subscription_id }) => {
  return wrapStripeRequest(stripeClient.subscriptions.del(subscription_id));
};

const routine_copyCustomersToTest = async () => {};

const routine_hydrateSubscriptions = async customer_id => {
  return wrapStripeRequest(
    stripeClient.customers.retrieve(customer_id, {
      api_key: secrets.stripeTokens.live.secret
    })
  );
};

module.exports = {
  reportCharges,
  reportActivePlans,
  purgeUsers,
  customersByEmail,
  customersById,
  attachSubscription,
  detachSubscripiton,
  routine_hydrateSubscriptions,
  customersByPlan
};
